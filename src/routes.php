<?php


// get courses

$app->get('/courses', function ($request, $response, $args) {
         $sth = $this->db->prepare("SELECT * FROM cursos ORDER BY id");
        $sth->execute();
        $cursos = $sth->fetchAll();
        return $this->response->withJson($cursos);
});

// add a new course

$app->post('/course', function ($request, $response) {
        $input = $request->getParsedBody();
        $sql = "INSERT INTO cursos (nome) VALUES (:nome)";
         $sth = $this->db->prepare($sql);
        $sth->bindParam("nome", $input['nome']);
        $sth->execute();
        $input['id'] = $this->db->lastInsertId();
        return $this->response->withJson($input);
});

// update course with given id

    $app->put('/courses/[{id}]', function ($request, $response, $args) {
        $input = $request->getParsedBody();
        $sql = "UPDATE cursos SET nome=:nome WHERE id=:id";
         $sth = $this->db->prepare($sql);
        $sth->bindParam("id", $args['id']);
        $sth->bindParam("nome", $input['nome']);
        $sth->execute();
        $input['id'] = $args['id'];
        return $this->response->withJson($input);
    });

// delete a course with given id

    $app->delete('/course/[{id}]', function ($request, $response, $args) {
         $sth = $this->db->prepare("DELETE FROM cursos WHERE id=:id");
        $sth->bindParam("id", $args['id']);
        $sth->execute();
        $cursos = $sth->fetchAll();
        return $this->response->withJson($cursos);
    });